A production-ready Django powered CMS website, running on Nginx, Gunicorn. And using the magic of Docker for better workflow and development to production.

First go into my_project folder where the Dockerfile is location and built this docker image:
docker build . -t my_project/backend
Then head into nginx to do the same:
docker build . -t my_project/backend-ingress

Before starting the server with docker compose up, don't forget to rename "env_variables" to ".env" and add your variables.
